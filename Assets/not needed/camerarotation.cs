﻿
using UnityEngine;

public class camerarotation : MonoBehaviour
{
    //mouse direction x y
    public Vector2 md;

    public Transform mybody;

    void Start()
    {
        mybody = this.transform.parent.transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mc = new Vector2
           (Input.GetAxisRaw("Mouse X"),
             -1 * Input.GetAxisRaw("Mouse Y"));

        md += mc;

        this.transform.localRotation =
            Quaternion.AngleAxis(md.y, Vector3.right);

        mybody.localRotation =
            Quaternion.AngleAxis(md.x, Vector3.up);


    }
}
