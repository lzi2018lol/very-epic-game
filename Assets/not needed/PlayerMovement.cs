﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody rb;

    public float forwardForce = 15f;
    public float sidewaysForce = 10f;
    public float backwardsForce = 10f;
    public float jumpForce = 10f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKey("d"))
        {
            rb.velocity = transform.right * sidewaysForce;
        }

        if (Input.GetKey("a"))
        {
            rb.velocity = -transform.right * sidewaysForce;
        }

        if (Input.GetKey("w"))
        {
            rb.velocity = transform.forward * forwardForce;
        }

        if (Input.GetKey("s"))
        {
            rb.velocity = -transform.forward * backwardsForce;
        }

        if (Input.GetKey("space"))
        {
            rb.velocity = transform.up * jumpForce;
        }

        if (rb.position.y < -3f)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

    }
}

